require 'rails_helper'

RSpec.feature "ProductsFeatures", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) do
    create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root)
  end
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "About taxonomy" do
    before { visit potepan_product_path(product.id) }

    scenario "The display on the product page is correct" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title h2", text: product.name
      expect(page).to have_selector ".col-xs-6 h2", text: product.name
      expect(page).to have_selector ".media-body h2", text: product.name
      expect(page).to have_selector ".media-body h3", text: product.display_price
      expect(page).to have_selector ".media-body p", text: product.description
    end

    scenario "The link to '一覧ページに戻る' is correct" do
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
    end

    scenario "It is possible to page transition when clicked product detail page" do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end

    scenario "Click on a related product to move to the product page" do
      within ".productsContent" do
        click_on related_products.first.name
      end
      expect(current_path).to eq potepan_product_path(related_products.first.id)
    end

    scenario "The names and prices of related products are displayed" do
      within ".productsContent" do
        expect(page).to have_content related_products.first.name
        expect(page).to have_content related_products.first.display_price
      end
    end

    scenario "Get 5 related products, 4 related products are displayed" do
      within ".productsContent" do
        expect(page).to have_selector ".productBox", count: 4
      end
    end
  end
end
